// I made a class for some math operations and named it MathOperations
class MathOperations {
    /* GCD function finds the Greatest Common Divisor between two numbers.
     * The function takes two arguments. Firstly, I made a variable named gcd,
     * then the divider. While divider is less than both numbers, function checks if both
     * number is divided by divider without remainders. If so, gcd is equal to divider. After that
     * divider grows by one. This process goes on until while condition is true. In the end,
     * the gcd is returned. */
    fun GCD(num1: Int, num2: Int): Int {
        var gcd = 0
        var divider = 1
        while (divider <= num1 && divider <= num2) {
            if (num1 % divider == 0 && num2 % divider == 0) {
                gcd = divider
            }
            divider += 1

        }
        return gcd
    }

    /* LCM function finds the Least Common Multiple of two numbers. I used the formula
     * to calculate it. When you want to find lcm of two numbers, you can multiply those
     * numbers and divide them by their gcd. I used previous GCD function in this
     * function. LCM function returns the least common multiple. */
    fun LCM(num1: Int, num2: Int): Int {
        val leastCommonMultiple = num1 * num2 / GCD(num1, num2)
        return leastCommonMultiple

    }

    /* This function checks if the string contains "$" symbol. I used contains() function to
     * find out if string had "$" in it. If condition is true, function returns that string contains "$",
     * otherwise, it returns that string does not contain"$". */
    fun checkString(word: String): String {
        return if (word.contains("$")) {
            "$word - contains $"
        } else {
            "$word - does not contain $"
        }
    }

    /* This function counts the sum of all even numbers between the given argument and 100.
     * Firstly, function checks if argument is even or not, if it is odd, function uses recursion
     * to check if next number is even. If it is even, addition is done between that number and
     * that number grown by 2. Else if condition checks if number is 100, if so, function returns 100 and. Which
     * is added to the sum. After num is 100 or more, function stops. (It returns definite value) */
    fun evenNumbers(num: Int): Int {
        if (num % 2 == 1) {
            return evenNumbers(num + 1)
        } else if (num >= 100) {
            return 100
        }
        return num + evenNumbers(num + 2)
    }

    /* reverseNum function reverses the given number. Firstly, I gave the num variable value of
     * number, because number could not be altered (It is defined by val). Second variable is reversedNumber.
     * While num variable is not 0, variable digit is equal to num % 10 remainder. Then reversedNumber is equalized
     * to itself multiplied by 10 and grown by previous digit variable's value. Then num's value is divided by 10,
     * which basically chops of last digit of the number. This process goes on until num = 0. Function returns
     * reversedNumber. */
    fun reverseNum(number: Int): Int {
        var num = number
        var reversedNumber = 0
        while (num != 0) {
            val digit = num % 10
            reversedNumber = reversedNumber * 10 + digit
            num /= 10
        }

        return reversedNumber
    }

    /* This function checks whether given string is palindrome or not. Firstly, I replaced every space, which stuck
     * every character together (This will make it easier to check equation). Afterwards, I reversed the word and
     * replaced every space in that reversed variable too. Lastly, if operator checks the equation between
     * word and its reversed version (It does not matter whether they are in UpperCase or LowerCase,
     * because it is said to be ignored in equal's function) and returns appropriate texts. */
    fun checkPalindrome(word: String): String {
        val newWord = word.replace(" ", "")
        val reversedWord = word.reversed()
        val newReversedWord = reversedWord.replace(" ", "")
        return if (newWord.equals(newReversedWord, ignoreCase = true)) {
            "$word - is a palindrome"
        } else {
            "$word - is not a palindrome"
        }
    }
}