fun main() {
    // I made an MathOperator Class object so that I can have access to class's functions
    val mathOperationObject = MathOperations()
    val checkGcd = mathOperationObject.GCD(12, 15)
    val checkLcm = mathOperationObject.LCM(3, 31)

    /* Because "$" is used in string formatting, to use it in your strings, you need to find other way around it.
     * One of it is using "\" symbol before writing dollar sign. Or """${$}""" This way is appropriate too. */
    val strChecker = mathOperationObject.checkString("Dilas leqciebisas neta chama\$ shegvedzlos..")

    // This function will return sum of even numbers from 1 to 100(including 100)
    val sumOfEvenNum = mathOperationObject.evenNumbers(1)
    val reverseMyNum = mathOperationObject.reverseNum(1994)
    val palindromeWord = mathOperationObject.checkPalindrome("Able was I ere I saw Elba")

    println(checkGcd)
    println(checkLcm)
    println(strChecker)
    println(sumOfEvenNum)
    println(reverseMyNum)
    print(palindromeWord)
}